<?php

namespace App\Console\Commands;

use App\User as UserCreate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Console\Command;

class user extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    protected $validator;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating user...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function show_error($variable){
        return $this->info($this->validator->messages()->first($variable));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $data['name'] = $this->ask('What is your name?');
        $this->validator = Validator::make($data, ['name' => 'required|max:255']);
        while ($this->validator->fails()){
             $this->show_error('name');
             $data['name'] = $this->ask('What is your name?');
             $this->validator = Validator::make($data, ['name' => 'required|max:255']);
        }

        $data['email'] = $this->ask('What is your email?');
        $this->validator = Validator::make($data, ['email' => 'required|email|max:255|unique:users']);
        while ($this->validator->fails()){
             $this->show_error('email');
             $data['email'] = $this->ask('What is your email?');
             $this->validator = Validator::make($data, ['email' => 'required|email|max:255|unique:users']);
        }

        $data['password'] = $this->secret('What is your password?');
        $this->validator = Validator::make($data, ['password' => 'required|min:6']);
        while ($this->validator->fails()){
            $this->show_error('password');
            $data['password'] = $this->secret('What is your password?');
            $this->validator = Validator::make($data, ['password' => 'required|min:6']);
        }

        $data['admin'] = $this->confirm('Create this user as admin?');

        if(UserCreate::create(['name' => $data['name'], 'email' => $data['email'],'password' => bcrypt($data['password']), 'admin' => $data['admin'],])){
            $this->info('User has been created successfully!');
        }
        else{
            $this->info('An error has occurred while creating a user!');
        }
    }
}
