<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['name'];

     public function feeds(){
		return $this->hasMany('App\Feeds');
	}
}
