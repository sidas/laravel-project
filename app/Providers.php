<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
	protected $fillable = ['id', 'name', 'url','icons_url', 'active'];

    public function feeds(){
		return $this->hasMany('App\Feeds');
	}

}
