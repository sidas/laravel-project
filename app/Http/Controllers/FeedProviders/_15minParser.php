<?php

namespace App\Http\Controllers\FeedProviders;

use Illuminate\Http\Request;
use App\Providers as Providers;
use App\Feeds as Feeds;
use App\Categories as Categories;

class _15minParser
{
	protected $providers_id = 1;
	protected $name = "15min";
	protected $url = "http://15min.lt";
	protected $icons_url = "";
	protected $active = 1;

    public function generate(){
    	$Feeds = new Feeds();
    	$Categories = new Categories();
        $location = $this->url."/rss";
		$xml = simplexml_load_file($location,null,LIBXML_NOCDATA);
		$articles = array();
		foreach ($xml->channel->item as $item) {
			if(empty($Categories->where('name', $item->category)->first())){
				$Categories->create(['name' => $item->category]);
			}
			$category = $Categories->where('name', $item->category)->first();
			$article = array(
				'providers_id' => $this->providers_id,
				'categories_id' => $category->id,
				'title' => (string) $item->title,
				'content' => trim(str_replace('Skaitykite daugiau...','',strip_tags((string)$item->description))),
				'url' => (string) $item->guid,
				'published' => date("Y-m-d H:i:s",strtotime((string)$item->pubDate))
			);
			if(empty($Feeds->where('url', $article['url'])->first())){
				$Feeds->create($article);
			}
		}
    }


    public function prepareProvider()
    {
    	$data = array(
    		'id' => $this->providers_id,
    		'name' => $this->name,
    		'url' => $this->url,
    		'icons_url' => $this->icons_url,
    		'active' => $this->active
    	);

    	return $data;
    }


    public function getProvidersID(){
    	return $this->providers_id;
    }
}
