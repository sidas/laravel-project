<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers as Providers;
use App\Feeds as Feeds;
use App\Categories as Categories;
use App\Http\Controllers\FeedProviders\_15minParser;
use App\Http\Controllers\FeedProviders\_LrytasParser;

class ProvidersController extends Controller
{
    public function providers_list(){
    	$Providers = Providers::all();
		return view('providers_list',array('Providers' => $Providers));
    }

    public function provider_view($id){
    	$Provider = Providers::find($id);
		$Feeds = $Provider->feeds;
        foreach ($Feeds as &$Feed) {
            $Feed->category = Categories::find($Feed->categories_id);
        }
		return view('provider_view',array('Provider' => $Provider, 'Feeds' => $Feeds));
    }

    public function generateAll(){
    	$_15minParser = new _15minParser();
        if(!Providers::find( $_15minParser->getProvidersID())){
            Providers::create($_15minParser->prepareProvider());
        }
    	$_15minParser->generate();

        $_LrytasParser = new _LrytasParser();
        if(!Providers::find( $_LrytasParser->getProvidersID())){
            Providers::create($_LrytasParser->prepareProvider());
        }
        $_LrytasParser->generate();
    }
}
