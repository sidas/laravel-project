<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function edit()
    {
        return view('edit');
    }

    public function validator($user){
        return Validator::make(Input::all(), [
            'password' => 'required|min:6|confirmed',
            'password_comfirmation' => 'same:password',
            'password_new' => 'required|min:6',
        ]);
    }

    public function postReset()
    {       
            $user = \Auth::user();
            $validator = $this->validator($user);
            if(!$validator->fails()){
                if(\Hash::check(Input::get('password_new'), $user->password)){
                    $user->password = bcrypt(Input::get('password_new'));
                    $user->save();
                    return redirect('/admin');
                }
                else{
                    $validator->getMessageBag()->add('password_new', 'Password wrong!');
                    return redirect('/profile_edit')->withErrors($validator);
                }
            }
            return redirect('/profile_edit')->withErrors($validator);
    }
}
