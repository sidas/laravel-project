<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use App\Feeds as Feeds;
use App\Providers as Providers;
use App\Categories as Categories;

class FeedsController extends Controller
{

	public function create(){
		$Categories = Categories::all();
		$Providers = Providers::all();
		return view('feed_create',array('Categories' => $Categories,'Providers' => $Providers));
    }

    public function delete(){
    	$id = (int)Input::get('id');
    	$Feed = Feeds::find($id);
  		if(!empty($Feed)){
  			Feeds::where('id','=',$id)->delete();
  		}
  		return redirect('/');
    }

    public function edit($id = 0){
		$Feed = Feeds::find($id);
		$Categories = Categories::all();
		$Providers = Providers::all();
		return view('feed_edit',array('Feed' => $Feed,'Categories' => $Categories,'Providers' => $Providers));
    }

    public function editPost()
    {
    	$e = Input::get('e_value');
    	switch ($e) {
    		case 'save':
    				return $this->save();
    			break;
    		case 'delete':
    				return $this->delete();
    			break;
    		default:
    				return redirect('/');
    			break;
    	}
    }

    public function feed_list(){
		$Feeds = Feeds::all()->sortByDesc('published');
        $Categories = Categories::all();
		return view('feed_list',array('Feeds' => $Feeds,'Categories' => $Categories));
    }

    public function validator(){
        return Validator::make(Input::all(), [
            'title' => 'required|min:6',
            'content' => 'required',
            'url' => 'required|url',
            'provider' => 'required',
            'category' => 'required',
            'published' => 'required|date',
        ]);
    }

    public function save(){
    	$id = (int)Input::get('id');

    	$validator = $this->validator();
    	if(!$validator->fails()){
    		if($id > 0){
	    		$Feed = Feeds::find($id);
	    		if(empty($Feed)){
	    			return redirect('/');
	    		}

	    		$Feed->title = Input::get('title');
	    		$Feed->content = Input::get('content');
	    		$Feed->url = Input::get('url');
	    		$Feed->providers_id = Input::get('provider');
	    		$Feed->categories_id = Input::get('category');
	    		$Feed->save();
	    	}
	    	else{
		    	$newFeed = array(
		    		'providers_id' => Input::get('provider'),
		    		'categories_id' => Input::get('category'),
		    		'title' => Input::get('title'),
		    		'content' => Input::get('content'),
		    		'url' => Input::get('url'),
		    		'published' => Input::get('published')
		    	);

		    	Feeds::create($newFeed);
	    	}
	    	return redirect('/');
    	}

    	return redirect()->back()->withErrors($validator);
    }

    public function sendJson($id = 0){
		$Feed = Feeds::find($id);
		return response()->json($Feed);
    }
}
