<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use App\Providers as Providers;
use App\Feeds as Feeds;
use App\Categories as Categories;

class CategoriesController extends Controller
{
    public function categories_list(){
    	$Categories = Categories::all();
		return view('categories_list',array('Categories' => $Categories));
    }

    public function category_view($id){
    	$Category = Categories::find($id);
		$Feeds = $Category->feeds;
        foreach ($Feeds as &$Feed) {
            $Feed->provider = Providers::find($Feed->providers_id);
        }
		return view('category_view',array('Category' => $Category, 'Feeds' => $Feeds));
    }

    public function create(){
        $Categories = Categories::all();
        return view('category_create',array('Categories' => $Categories));
    }

    public function delete(){
        $id = (int)Input::get('id');
        $Category = Categories::find($id);
        if(!empty($Category)){
            Feeds::where('categories_id','=',$id)->delete();
            Categories::where('id','=',$id)->delete();
        }
        return redirect('/');
    }

    public function edit($id = 0){
        $Category = Categories::find($id);
        return view('category_edit',array('Category' => $Category));
    }

    public function editPost()
    {
        $e = Input::get('e_value');
        switch ($e) {
            case 'save':
                    return $this->save();
                break;
            case 'delete':
                    return $this->delete();
                break;
            default:
                    return redirect('/');
                break;
        }
    }

    public function save(){
        $id = (int)Input::get('id');

        $validator = $this->validator();
        if(!$validator->fails()){
            if($id > 0){
                $Category = Categories::find($id);
                if(empty($Category)){
                    return redirect('/');
                }

                $Category->name = Input::get('name');
                $Category->save();
            }
            else{
                $newFeed = array(
                    'name' => Input::get('name'),
                );

                Categories::create($newFeed);
            }
            return redirect('/');
        }

        return redirect()->back()->withErrors($validator);
    }


    public function validator(){
        return Validator::make(Input::all(), [
            'name' => 'required|min:6'
        ]);
    }
}