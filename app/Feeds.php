<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
	protected $fillable = ['providers_id', 'categories_id', 'title','pics_url', 'content','url','published'];

    public function provider(){
		return $this->belongsTo('App\Providers','providers_id');
	}

	public function category(){
		return $this->belongsTo('App\Categories','categories_id');
	}
}