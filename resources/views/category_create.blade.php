@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create new category</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" name="feed" method="POST" action="{{ url('/category_edit') }}">
                        <input id="e_value" name="e_value" type="hidden" value="save"/>
                        <input id="id" name="id" type="hidden" value="0"/>
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button id="save" type="button" class="btn btn-primary">
                                    Save
                                </button>
                                <button id="cancel" type="button" class="btn btn-danger">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
    jQuery('#save').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('save');
        jQuery('form[name="feed"]').submit();
    });
    jQuery('#cancel').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('cancel');
        jQuery('form[name="feed"]').submit();
    });
    jQuery('#delete').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('delete');
        jQuery('form[name="feed"]').submit();
    });
</script>
@endsection