@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All feeds 
                   @if (Auth::check('isAdmin'))
                    <a href="{{ url('/feed_create') }}">Create new feed</a>
                  @endif
                </div>
                <div class="panel-body">
                   <div class="row">
                      <div class="col-md-4">
                        Filter by category:
                      </div>
                      <div class="col-md-4">
                        <select id="category" name="category" class="form-control" required="">
                            <option value="0">All</option>
                            @foreach($Categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                    <br/><br/>

          					@foreach ($Feeds as $feed)
          						<div class="article" data-category="{{ $feed->category->id }}">
          							<div class="name"><a class="id-{{ $feed->provider->id }}" href="{{ $feed->provider->url }}" target="_blank">{{ $feed->provider->name }}</a> <span class="category">{{ $feed->category->name }}</span></div>
          							<div class="title"> <a class="open btn btn-link" data-id="{{ $feed->id }}" >{{ $feed->title }}</a></div>
          							<div class="published">{{ $feed->published }} @if (Auth::check('isAdmin')) <br/><a href="{{ url('/feed_edit/'.$feed->id) }}">Edit this feed</a> @endif</div>
          						</div>
          						<div class="clear"></div>
          					@endforeach
 				        </div>
            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="feedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a class="link" target="_blank" href="#" class="btn btn-primary">Go to feed page</a>
      </div>
    </div>
  </div>
</div>
@endsection



@section('javascript')
<script type="text/javascript">
  jQuery('.article .title .open').on('click',function(){
    var id = jQuery(this).attr('data-id');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    jQuery.ajax({
         type: "POST",
         url: "{{ config('app.url') }}/feed_info/"+id,
         data: {_token: CSRF_TOKEN},
         dataType: "json",
         success: function (data) {
            jQuery('#feedModal .modal-title').html(data.title);
            jQuery('#feedModal .modal-body').html(data.content);
            jQuery('#feedModal .modal-body').append('<br/>'+data.published);
            jQuery('#feedModal .link').attr('href',data.url);
            jQuery('#feedModal').modal('show');
         },
     });
  });

  jQuery('#category').on('change',function(){
    var value = jQuery(this).val();
    if(value>0){
      jQuery('.article').each(function(){
        jQuery(this).show();
        if(jQuery(this).attr('data-category') != value){
          jQuery(this).hide();
        }
      });
    }
    else{
      jQuery('.article').each(function(){
        jQuery(this).show();
      });
    }
  });
</script>
@endsection