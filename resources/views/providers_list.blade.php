@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Providers list</div>
                <div class="panel-body">
					<table class="list_table" border="1" width="100%">
						<tr>
							<th width="50" class="show-for-large-up">id</th>
							<th>name</th>
						</tr>
						@foreach($Providers as $provider)
						<tr>	
							<td>{{ $provider->id }}</td> 
							<td><a href="{{ url('/provider_view/'.$provider->id) }}" >{{ $provider->name }}</a></td>
						</tr>
						@endforeach
					</table>
 				 </div>
            </div>
        </div>
    </div>
</div>
@endsection