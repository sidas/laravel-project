@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$Provider->name}} Providers feed list</div>
                <div class="panel-body">
					<table class="list_table" border="1" width="100%">
						<tr>
							<th width="50" class="show-for-large-up">id</th>
							<th>category</th>
							<th>title</th>
							<th>short desc</th>
							<th width="90">action</th>
						</tr>
						@foreach($Provider->feeds as $feed)
						<tr>	
							<td>{{ $feed->id }}</td> 
							<td><a href="{{ url('/category_view/'.$feed->categories_id) }}" >{{ $feed->category->name }}</a></td>
							<td>{{ $feed->title }}</td>
							<td>{!! $feed->content !!}</td>
							<td><a href="#" >Edit</a></td>
						</tr>
						@endforeach
					</table>
 				 </div>
            </div>
        </div>
    </div>
</div>
@endsection