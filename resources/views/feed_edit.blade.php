@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit this feed</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" name="feed" method="POST" action="{{ url('/feed_edit') }}">
                        <input id="e_value" name="e_value" type="hidden" value="save"/>
                        <input id="id" name="id" type="hidden" value="{{ $Feed->id }}"/>
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ $Feed->title }}" required>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea id="content" class="form-control" name="content" required>{{ $Feed->content }}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url" class="col-md-4 control-label">Url</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control" name="url" value="{{ $Feed->url }}" required>

                                @if ($errors->has('url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('provider') ? ' has-error' : '' }}">
                            <label for="provider" class="col-md-4 control-label">Provider</label>

                            <div class="col-md-6">
                                <select id="provider" name="provider" class="form-control" required="">
                                    @foreach($Providers as $provider)
                                        <option value="{{$provider->id}}" {{ ($Feed->providers_id == $provider->id) ? 'selected' : '' }}>{{$provider->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('provider'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('provider') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Category</label>

                            <div class="col-md-6">
                                <select id="category" name="category" class="form-control" required="">
                                    @foreach($Categories as $category)
                                        <option value="{{$category->id}}" {{ ($Feed->categories_id == $category->id) ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('published') ? ' has-error' : '' }}">
                            <label for="published" class="col-md-4 control-label">Published date</label>

                            <div class="col-md-6">
                                <input id="published" type="text" class="form-control" name="published" value="{{ $Feed->published }}" required>

                                @if ($errors->has('published'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('published') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button id="save" type="button" class="btn btn-primary">
                                    Edit
                                </button>
                                <button id="cancel" type="button" class="btn btn-danger">
                                    Cancel
                                </button>
                                @if(!empty($Feed->id))
                                    <button id="delete" type="button" class="btn btn-link">
                                        Delete this feed?
                                    </button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
    jQuery('#save').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('save');
        jQuery('form[name="feed"]').submit();
    });
    jQuery('#cancel').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('cancel');
        jQuery('form[name="feed"]').submit();
    });
    jQuery('#delete').on('click',function(e){
        e.preventDefault();
        jQuery('#e_value').val('delete');
        jQuery('form[name="feed"]').submit();
    });
</script>
@endsection