@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Categories list</div>
                <div class="panel-body">
					<table class="list_table" border="1" width="100%">
						<tr>
							<th width="50" class="show-for-large-up">id</th>
							<th>name</th>
							<th width="90">action</th>
						</tr>
						@foreach($Categories as $category)
						<tr>	
							<td>{{ $category->id }}</td> 
							<td><a href="{{ url('/category_view/'.$category->id) }}" >{{ $category->name }}</a></td>
							<td><a href="{{ url('/category_edit/'.$category->id) }}" >Edit</a></td>
						</tr>
						@endforeach
					</table>
 				 </div>
            </div>
        </div>
    </div>
</div>
@endsection