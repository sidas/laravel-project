<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds',function(Blueprint $table){
            $table->increments('id');
            $table->integer('providers_id');
            $table->integer('categories_id');
            $table->string('title',120);
            $table->string('pics_url',120)->nullable();
            $table->longText('content');
            $table->longText('url');
            $table->dateTime('published');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feeds');
    }
}
