<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','FeedsController@feed_list');
Route::post('/feed_info/{id}','FeedsController@sendJson');

Route::group(['middleware' => 'web'],function(){
	Auth::routes();
	Route::get('/admin', 'HomeController@index');
	Route::get('/profile_edit', 'HomeController@edit')->middleware('isAdmin');
	Route::post('/profile_edit', 'HomeController@postReset')->middleware('isAdmin');

	Route::get('providers_list','ProvidersController@providers_list')->middleware('isAdmin');
	Route::get('provider_view/{id}','ProvidersController@provider_view')->middleware('isAdmin');
	Route::get('/categories_list','CategoriesController@categories_list')->middleware('isAdmin');
	Route::get('/category_view/{id}','CategoriesController@category_view')->middleware('isAdmin');
	Route::get('/category_edit/{id}','CategoriesController@edit')->middleware('isAdmin');
	Route::get('/category_create','CategoriesController@create')->middleware('isAdmin');
	Route::post('/category_edit','CategoriesController@editPost')->middleware('isAdmin');
	Route::get('/feed_update','ProvidersController@generateAll')->middleware('isAdmin');
	Route::get('/feed_edit/{id}','FeedsController@edit')->middleware('isAdmin');
	Route::get('/feed_create','FeedsController@create')->middleware('isAdmin');
	Route::post('/feed_edit','FeedsController@editPost')->middleware('isAdmin');
});
