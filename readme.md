#Laravel Feed project
To configure you should:
 * create laravel project by typing "laravel new your-project-name"
 
 * Then pulling all repository files
 
 * creating database
 
 * setting up .env database [ Also you must type app url example: http://localhost/laravel-project/public ]
 
 * use php artisan migrate command
 
 * create new user by typing "user:create"
 
 * update all feed by typing "feed:update"